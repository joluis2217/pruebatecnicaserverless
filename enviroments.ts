export const enviroment = {
    production: false,
    hmr: false,
  
    api: {
        SWAPI: 'https://swapi.py4e.com/api',
    },
    SWAPI:{
        getStarship:"/starships",
        getStarshipId:"/starships/{0}",
        getPeople:"/people",
        getPeopleId:"/people/{0}",
        getFilm:"/films",
        getFilmId:"/films/{0}"
    }
}