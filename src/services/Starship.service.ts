import axios, { AxiosResponse } from 'axios'; 
import {enviroment} from '../../enviroments';
import  {stringFormat} from '../util/util'

export class StarshipService {

  async  getStarships(): Promise<any> { 
    try {
      const url = enviroment.api.SWAPI+enviroment.SWAPI.getStarship; 
      const respuesta: AxiosResponse = await axios.get(url);
      const datos = respuesta.data;
      return datos; 
    } catch (error) {
      console.error('Error al obtener datos de la API:', error);
      throw error; 
    }
  }

  async  getStarshipId(id:String): Promise<any> { 
    try {
        const url = stringFormat(enviroment.SWAPI.getStarshipId, id) ; 
      const respuesta: AxiosResponse = await axios.get( enviroment.api.SWAPI+url);
      const datos = respuesta.data;
      return datos; 
    } catch (error) {
      console.error('Error al obtener datos de la API:', error);
      return null; 
    }
  }
  
    
    
    
}