import axios, { AxiosResponse } from 'axios'; 
import {enviroment} from '../../enviroments';
import  {stringFormat} from '../util/util'

export class FilmService {

  async  getFilms(): Promise<any> { 
    try {
      const url = enviroment.api.SWAPI+enviroment.SWAPI.getFilm; 
      const respuesta: AxiosResponse = await axios.get(url);
      const datos = respuesta.data;
      return datos; 
    } catch (error) {
      console.error('Error al obtener datos de la API:', error);
      throw error; 
    }
  }

  async  getFilmId(id:String): Promise<any> { 
    try {
        const url = stringFormat(enviroment.SWAPI.getFilmId, id) ; 
      const respuesta: AxiosResponse = await axios.get( enviroment.api.SWAPI+url);
      const datos = respuesta.data;
      return datos; 
    } catch (error) {
      console.error('Error al obtener datos de la API:', error);
      return null; 
    }
  }
  
    
    
    
}