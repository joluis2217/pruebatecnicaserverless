export class Person {
    anioNacimiento: string = '';
    colorOjos: string = '';
    peliculas: string[] = [];
    genero: string = '';
    colorCabello: string = '';
    altura: string = '';
    planetaNatal: string = '';
    peso: string = '';
    nombre: string = '';
    colorPiel: string = '';
    creado: string = '';
    editado: string = '';
    especies: string[] = [];
    navesEstelares: string[] = [];
    url: string = '';
    vehiculos: string[] = [];
    static mapDataToDTO(data: any): Person {
        const personDTO: Person = new Person();
        personDTO.anioNacimiento = data.birth_year || '';
        personDTO.colorOjos = data.eye_color || '';
        personDTO.peliculas = data.films || [];
        personDTO.genero = data.gender || '';
        personDTO.colorCabello = data.hair_color || '';
        personDTO.altura = data.height || '';
        personDTO.planetaNatal = data.homeworld || '';
        personDTO.peso = data.mass || '';
        personDTO.nombre = data.name || '';
        personDTO.colorPiel = data.skin_color || '';
        personDTO.creado = data.created || '';
        personDTO.editado = data.edited || '';
        personDTO.especies = data.species || [];
        personDTO.navesEstelares = data.starships || [];
        personDTO.url = data.url || '';
        personDTO.vehiculos = data.vehicles || [];
        return personDTO;
      }
  }