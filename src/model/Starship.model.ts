export class Starship {
    id:string='';
    MGLT: string = '';
    capacidadDeCarga: string = '';
    consumibles: string = '';
    costoEnCreditos: string = '';
    creado: string = '';
    tripulacion: string = '';
    editado: string = '';
    calificacionDelHiperimpulsor: string = '';
    longitud: string = '';
    fabricante: string = '';
    velocidadMaximaEnAtmosfera: string = '';
    modelo: string = '';
    nombre: string = '';
    pasajeros: string = '';
    peliculas: string[] = [];
    pilotos: string[] = [];
    claseDeNaveEstelar: string = '';
    url: string = '';
  
    static mapDataToDTO(data: any): Starship {
      const starshipDTO: Starship = new Starship();
      starshipDTO.MGLT = data.MGLT || '';
      starshipDTO.capacidadDeCarga = data.cargo_capacity || '';
      starshipDTO.consumibles = data.consumables || '';
      starshipDTO.costoEnCreditos = data.cost_in_credits || '';
      starshipDTO.creado = data.created || '';
      starshipDTO.tripulacion = data.crew || '';
      starshipDTO.editado = data.edited || '';
      starshipDTO.calificacionDelHiperimpulsor = data.hyperdrive_rating || '';
      starshipDTO.longitud = data.length || '';
      starshipDTO.fabricante = data.manufacturer || '';
      starshipDTO.velocidadMaximaEnAtmosfera = data.max_atmosphering_speed || '';
      starshipDTO.modelo = data.model || '';
      starshipDTO.nombre = data.name || '';
      starshipDTO.pasajeros = data.passengers || '';
      starshipDTO.peliculas = data.films || [];
      starshipDTO.pilotos = data.pilots || [];
      starshipDTO.claseDeNaveEstelar = data.starship_class || '';
      starshipDTO.url = data.url || '';
      return starshipDTO;
    }
  }