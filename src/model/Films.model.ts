export class Film {
    personajes: string[] = [];
    creado: string = '';
    director: string = '';
    editado: string = '';
    episodioId: number = 0;
    apertura: string = '';
    planetas: string[] = [];
    productor: string = '';
    fechaLanzamiento: string = '';
    especies: string[] = [];
    navesEstelares: string[] = [];
    titulo: string = '';
    url: string = '';
    vehiculos: string[] = [];
  

    static mapDataToDTO(data: any): Film {
        const peliculaDTO: Film = new Film();
        peliculaDTO.personajes = data.characters || [];
        peliculaDTO.creado = data.created || '';
        peliculaDTO.director = data.director || '';
        peliculaDTO.editado = data.edited || '';
        peliculaDTO.episodioId = data.episode_id || 0;
        peliculaDTO.apertura = data.opening_crawl || '';
        peliculaDTO.planetas = data.planets || [];
        peliculaDTO.productor = data.producer || '';
        peliculaDTO.fechaLanzamiento = data.release_date || '';
        peliculaDTO.especies = data.species || [];
        peliculaDTO.navesEstelares = data.starships || [];
        peliculaDTO.titulo = data.title || '';
        peliculaDTO.url = data.url || '';
        peliculaDTO.vehiculos = data.vehicles || [];
        return peliculaDTO;
      }

  }