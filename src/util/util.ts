
export function 
     stringFormat(template, ...values) {
    return template.replace(/{(\d+)}/g, (match, index) => {
      const value = values[index];
      return value !== undefined ? value : match;
    });
  }


