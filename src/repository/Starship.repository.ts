import {Starship} from '../model/Starship.model' 
import { v4 as uuidv4 } from "uuid";
import * as AWS from "aws-sdk";

export class StarshipRepository {
    
    async addStarship(starship:Starship){
        const dynamodb = new AWS.DynamoDB.DocumentClient();
        const createdAt = new Date();
        const id = uuidv4();
        starship.id=id;
        starship.creado=createdAt.toISOString();;
        await dynamodb
        .put({
          TableName: 'Starship',
          Item: starship,
        })
        .promise();
    }

}