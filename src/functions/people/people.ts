import {PeopleQuery} from '../../query/People.query' 
import { APIGatewayProxyResult,APIGatewayProxyEvent } from 'aws-lambda';


export const getPeople: APIGatewayProxyResult = async () => {
    const peopleQuery= new PeopleQuery();
        const data = await peopleQuery.getPeople();
        return {
          statusCode: 200,
          body: JSON.stringify(data)
        };
};

export const getPeopleId: APIGatewayProxyResult = async (event: APIGatewayProxyEvent) => {
  const peopleQuery= new PeopleQuery();
  const { id } = event.pathParameters || {}; // Lee el parámetro 'id' de la UR
      const data = await peopleQuery.getPeopleId(id);
      return {
        statusCode: 200,
        body: JSON.stringify(data)
      };
};


