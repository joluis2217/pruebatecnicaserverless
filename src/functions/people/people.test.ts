import { PeopleQuery } from '../../query/People.query';
import { PeopleService } from '../../services/People.service';
import { Person } from '../../model/People.model';


jest.mock('../../services/People.service');

describe('PeopleQuery', () => {
  let peopleQuery: PeopleQuery;

  beforeEach(() => {
    peopleQuery = new PeopleQuery();
  });

  it('getPeople', async () => {
    (PeopleService.prototype.getPeople as jest.Mock).mockResolvedValue({
      results: [
        {
            "name": "",
            "height": "",}],
    });

    const result = await peopleQuery.getPeople();
    expect(result).toEqual([Person.mapDataToDTO( {
        "nombre": "",
        "altura": "",})]);
  });

  it('getPeopleID', async () => {
    const mockFilmId = '1';
    (PeopleService.prototype.getPeopleId as jest.Mock).mockResolvedValue({
    });

    const result = await peopleQuery.getPeopleId(mockFilmId);
    expect(result).toEqual(Person.mapDataToDTO({ 
        "nombre": "Jose",
        "altura": "170"}));
  });
});