//import schema from './schema';
import { handlerPath } from '@libs/handler-resolver';

export default {
  handler: `${handlerPath(__dirname)}/starship.getStarships`,
  events: [
    {
      http: {
        method: 'get',
        path: 'starship',
        /*request: {
          schemas: {
            'application/json': schema,
          },
        },*/
      },
    },
  ],
};
