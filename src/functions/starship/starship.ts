import {StarshipQuery} from '../../query/Starship.query' 
import { APIGatewayProxyResult,APIGatewayProxyEvent } from 'aws-lambda';
import {StarshipRepository} from '../../repository/Starship.repository' 
import {Starship} from '../../model/Starship.model' 


export const getStarships: APIGatewayProxyResult = async () => {
    const starshipQuery= new StarshipQuery();
        const data = await starshipQuery.getStarships();
        return {
          statusCode: 200,
          body: JSON.stringify(data)
        };
};


export const addStarship: APIGatewayProxyResult = async (event: APIGatewayProxyEvent) => {
  try {
    const starshipRepository= new StarshipRepository();
    const newstarship: Starship = JSON.parse(event.body || "{}") as Starship; 
    await starshipRepository.addStarship(newstarship);
    return {
      statusCode: 200,
      body: JSON.stringify({ message: 'Starship agregada correctamente' }),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error: 'Error al agregar la Starship' }),
    };
  }
};

export const getStarshipId: APIGatewayProxyResult = async (event: APIGatewayProxyEvent) => {
  const starshipQuery= new StarshipQuery();
  const { id } = event.pathParameters || {}; // Lee el parámetro 'id' de la UR
      const data = await starshipQuery.getStarshipsId(id);
      return {
        statusCode: 200,
        body: JSON.stringify(data)
      };
};


