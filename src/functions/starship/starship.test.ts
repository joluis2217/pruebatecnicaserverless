import { addStarship, getStarships } from './starship';
import { APIGatewayProxyResult} from 'aws-lambda';
import { StarshipRepository } from '../../repository/Starship.repository';


jest.mock('../../query/Starship.query', () => {
  return {
    StarshipQuery: jest.fn().mockImplementation(() => ({
      getStarships: jest.fn().mockResolvedValue([{
        "id": "",
        "MGLT": "",
        "capacidadDeCarga": ""}]),
    })),
  };
});


describe('getStarships function', () => {
  it('starships', async () => {
    const result: APIGatewayProxyResult = await getStarships();
    expect(result.statusCode).toBe(200);
    expect(result.body).toEqual(JSON.stringify([{
        "id": "",
        "MGLT": "",
        "capacidadDeCarga": ""}]));
  });
});

jest.mock('../../repository/Starship.repository');
describe('addStarship function', () => {
  it('Starshipsuccessfully', async () => {
    const mockAddStarship = StarshipRepository.prototype.addStarship as jest.Mock;
    mockAddStarship.mockResolvedValue({ message: 'Starship agregada correctamente' });
    const event = {
      body: JSON.stringify({ 
        "MGLT": "",
        "capacidadDeCarga": "",
        "consumibles": "",
        "costoEnCreditos": "" }),
    };

    const result = await addStarship(event);

    expect(result.statusCode).toBe(200);
    expect(JSON.parse(result.body)).toEqual({ message: 'Starship agregada correctamente' });
    
    expect(mockAddStarship).toHaveBeenCalledWith(expect.any(Object));
  });

  it('errorsStarship', async () => {
    const mockAddStarship = StarshipRepository.prototype.addStarship as jest.Mock;
    mockAddStarship.mockRejectedValue(new Error('Error simulado al agregar la Starship'));

    const event = {
      body: JSON.stringify({  
        "MGSDLT": "",
        "capacidASDadDeCarga": "",
        "consuSDmibles": "",
        "costoAEnCreditos": ""  }),
    };

    const result = await addStarship(event);

    expect(result.statusCode).toBe(500);
    expect(JSON.parse(result.body)).toEqual({ error: 'Error al agregar la Starship' });
    expect(mockAddStarship).toHaveBeenCalledWith(expect.any(Object));
  });
});

