import {FilmQuery} from '../../query/Film.query' 
import { APIGatewayProxyResult,APIGatewayProxyEvent } from 'aws-lambda';


export const getFilms: APIGatewayProxyResult = async () => {
    const filmQuery= new FilmQuery();
        const data = await filmQuery.getFilms();
        return {
          statusCode: 200,
          body: JSON.stringify(data)
        };
};

export const getFilmId: APIGatewayProxyResult = async (event: APIGatewayProxyEvent) => {
  const filmQuery= new FilmQuery();
  const { id } = event.pathParameters || {};
      const data = await filmQuery.getFilmId(id);
      return {
        statusCode: 200,
        body: JSON.stringify(data)
      };
};


