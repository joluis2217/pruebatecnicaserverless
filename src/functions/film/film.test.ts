import { FilmQuery } from '../../query/Film.query';
import { FilmService } from '../../services/Film.service';
import { Film } from '../../model/Films.model';


jest.mock('../../services/Film.service');

describe('FilmQuery', () => {
  let filmQuery: FilmQuery;

  beforeEach(() => {
    filmQuery = new FilmQuery();
  });

  it('getFilms', async () => {
    (FilmService.prototype.getFilms as jest.Mock).mockResolvedValue({
      results: [{ 
        "title": "",
        "episode_id": 0}],
    });

    const result = await filmQuery.getFilms();
    expect(result).toEqual([Film.mapDataToDTO({ 
        "titulo": "",
        "episodioId": 0})]);
  });

  it('getFilmID', async () => {
    const mockFilmId = '1';
    (FilmService.prototype.getFilmId as jest.Mock).mockResolvedValue({
    });

    const result = await filmQuery.getFilmId(mockFilmId);
    expect(result).toEqual(Film.mapDataToDTO({ 
        "titulo": "A New Hope",
        "episodioId": "4"}));
  });
});