import { FilmService } from '../services/Film.service';
import {Film} from '../model/Films.model' 
export class FilmQuery {

    async  getFilms(): Promise<any> { 

        const filmService = new FilmService();
        const filmsData = await filmService.getFilms();
        const filmInstances = (filmsData.results||[]).map((filmData: any) => Film.mapDataToDTO(filmData));
        return filmInstances; 
    }

    async  getFilmId(id:String): Promise<any> {         

        const filmService = new FilmService();
        const filmData = await filmService.getFilmId(id);
        const filmInstances = filmData ? Film.mapDataToDTO(filmData) : null;
        return filmInstances?filmInstances:'{}'; 
    }

    
}