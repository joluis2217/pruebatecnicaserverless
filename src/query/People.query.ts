import { PeopleService } from '../services/People.service';
import {Person} from '../model/People.model' 
export class PeopleQuery {

    async  getPeople(): Promise<any> { 
        const peopleService = new PeopleService();
        const peopleData = await peopleService.getPeople();
        const peopleInstances = (peopleData.results||[]).map((personData: any) => Person.mapDataToDTO(personData));
        return peopleInstances; 
    }

    async  getPeopleId(id:String): Promise<any> {         
        const peopleService = new PeopleService();
        const peopleData = await peopleService.getPeopleId(id);
        const peopleInstances = peopleData ? Person.mapDataToDTO(peopleData) : null;
        return peopleInstances?peopleInstances:'{}'; 
    }

    
}