import { StarshipService } from '../services/Starship.service';
import {Starship} from '../model/Starship.model' 
import * as AWS from "aws-sdk";

export class StarshipQuery {

    async  getStarships(): Promise<any> { 
        const dynamodb = new AWS.DynamoDB.DocumentClient();
        const result=
        await dynamodb
        .scan({
          TableName: 'Starship'
        })
        .promise();

        const startshipDb=result.Items;
        const starshipService = new StarshipService();
        const starshipsData = await starshipService.getStarships();
        const starshipInstances = (starshipsData.results||[]).map((starshipData: any) => Starship.mapDataToDTO(starshipData));
        const allStarships = startshipDb.concat(starshipInstances);

        return allStarships; 
    }

    async  getStarshipsId(id:String): Promise<any> { 
        const dynamodb = new AWS.DynamoDB.DocumentClient();
        const result=
        await dynamodb
        .get({
          TableName: 'Starship',
          Key:{
            id
          }
        })
        .promise();
        
        const startshipDb=result.Item?result.Item:null;
        const starshipService = new StarshipService();
        const starshipData = await starshipService.getStarshipId(id);
        const starshipInstances = starshipData ? Starship.mapDataToDTO(starshipData) : null;
        return startshipDb?startshipDb:starshipInstances?starshipInstances:'{}'; 
    }

    
}