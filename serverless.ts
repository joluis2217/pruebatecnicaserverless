import 'tsconfig-paths/register';
import type { AWS } from '@serverless/typescript';

const serverlessConfiguration: AWS = {
  service: 'serverlesspruebatecnica',
  frameworkVersion: '3',
  plugins: ['serverless-auto-swagger','serverless-esbuild'],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    region:'us-east-1',
    iamRoleStatements: [
      {
        Effect: 'Allow',
        Action: ['dynamodb:*'],
        Resource: 'arn:aws:dynamodb:us-east-1:820070041530:table/Starship',
      },
    ],
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true

    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
      
    },
  },
  resources: {
    Resources: {
      Starship: {
        Type: 'AWS::DynamoDB::Table',
        Properties: {
          TableName: 'Starship',
          BillingMode: 'PAY_PER_REQUEST',
          AttributeDefinitions: [
            {
              AttributeName: 'id',
              AttributeType: 'S',
            },
          ],
          KeySchema: [
            {
              AttributeName: 'id',
              KeyType: 'HASH',
            },
          ],
        },
      },
    },
  },
  functions: { 
    getStarships:{
      handler: 'src/functions/starship/starship.getStarships',
        events: [
          {
            http: {
              method: 'get',
              path: 'starship'
            },
          },
        ],
    },
    addStarship:{
      handler: 'src/functions/starship/starship.addStarship',
        events: [
          {
            http: {
              method: 'post',
              path: 'starship'
            },
          },
        ],
    },
    getStarshipId:{
      handler: 'src/functions/starship/starship.getStarshipId',
        events: [
          {
            http: {
              method: 'get',
              path: 'starship/{id}'
            },
          },
        ],
    },
    getFilms:{
      handler: 'src/functions/film/film.getFilms',
        events: [
          {
            http: {
              method: 'get',
              path: 'film'
            },
          },
        ],
    },
    getFilmId:{
      handler: 'src/functions/film/film.getFilmId',
        events: [
          {
            http: {
              method: 'GET',
              path: 'film/{id}'
            },
          },
        ],
    },
    getPeople:{
      handler: 'src/functions/people/people.getPeople',
        events: [
          {
            http: {
              method: 'get',
              path: 'people'
            },
          },
        ],
    },
    getPeopleId:{
      handler: 'src/functions/people/people.getPeopleId',
        events: [
          {
            http: {
              method: 'get',
              path: 'people/{id}'
            },
          },
        ],
    },
},
  package: { individually: true },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
    },
    swagger: {
      basePath: 'https://28i1a02vnb.execute-api.us-east-1.amazonaws.com/dev',
    },
  },
 
};

module.exports = serverlessConfiguration;
